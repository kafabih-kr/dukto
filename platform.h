/* DUKTO - A simple, fast and multi-platform file transfer tool for LAN users
 * Copyright (C) 2011 Emanuele Colombo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef PLATFORM_H
#define PLATFORM_H

#include <qglobal.h>
#include "settings.h"
#ifdef Q_OS_ANDROID
#include <jni.h>
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
#include <QtCore/private/qandroidextras_p.h>
#include <QJniObject>
#include <QJniEnvironment>
#else
#include <QtAndroid>
#endif
#endif
#include <QObject>

class Settings;

class ActivityHandler;

class Platform : public QObject
{
public:
    explicit Platform(QObject *parent = nullptr);
    QString getSystemUsername();
    QString getHostname();
    QString getPlatformName();
    QString getAvatarPath();
    QString getDefaultPath();
    bool isArchlinux();
#ifdef Q_OS_ANDROID
    int getAndroidSDKVersion();
    bool requestAndroidPermissions();
    bool findAndroidMiui();
    QString uriAndroidParser(QString path, bool reverse = false);
    QString getAndroidDataPath();
    bool findAndroidTv();
    void setFitSystemWindow();
    bool openAndroidFile(QString file, bool isdir = false);
    bool findNavbar();
    bool findAndroidDocUI();
#endif
    
#if defined(Q_OS_LINUX)
    QString getLinuxAvatarPath();
#elif defined(Q_OS_MAC)
    QString getMacTempAvatarPath();
#elif defined(Q_OS_WIN)
    QString getWinTempAvatarPath();
#endif
    int getNativeWidth();
    int getNativeHeight();
    int getMediumWidth();
    int getMediumHeight();
    int getNFWidth();
    int getNFHeight();
    int getTKWidth();
    int getTKHeight();
private:
    Settings *settings;
protected:
#ifdef Q_OS_ANDROID
    QJniObject initActivity();
    bool findAppInstalled(QString app);
    QString findAndroidProps(QString props);
#endif
    QString runCommand(QString command);
    QString capitalise(const QString& sentence);
};

#endif // PLATFORM_H
