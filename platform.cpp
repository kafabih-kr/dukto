/* DUKTO - A simple, fast and multi-platform file transfer tool for LAN users
 * Copyright (C) 2011 Emanuele Colombo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "platform.h"
#include "settings.h"

#include <QString>
#include <QHostInfo>
#include <QFile>
#include <QDir>
#include <QUrl>
#include <QByteArray>
#include <QRegularExpression>
#include <QProcess>
#include <QStandardPaths>
#if QT_VERSION <= QT_VERSION_CHECK(4,8,0)
#include <QSystemDeviceInfo>
#else
#include <QSysInfo>
#endif

#if defined(Q_OS_MAC)
#include <QTemporaryFile>
#include <CoreServices/CoreServices.h>
#endif

#if defined(Q_OS_WIN)
#include <windows.h>
#include <lmaccess.h>
#endif

#if defined(Q_WS_S60)
#define SYMBIAN
QTM_USE_NAMESPACE
#endif

#if defined(Q_WS_SIMULATOR)
#define SYMBIAN
#endif
#ifdef Q_OS_ANDROID
#include <QApplication>
#include <QTimer>
#endif
#if QT_VERSION >= QT_VERSION_CHECK(5,13,0)
#include <QGuiApplication>
#include <QScreen>
#else
#include <QDesktopWidget>
#endif

Platform::Platform(QObject *parent) :
    QObject(parent)
{
}

// Returns the system username
QString Platform::getSystemUsername()
{
    static QString username;
#if defined(SYMBIAN)
    // Get username from settings
    username = settings->buddyName();
#else
    QString product = QSysInfo::productType();
    if(product == "android" || product == "ios"){
        username = capitalise(QSysInfo::kernelType())+" on "+
        capitalise(product)+" "+QSysInfo::productVersion();
    } else {
    // Looking for the username
        QString uname = qgetenv("USER");
        QString usname = qgetenv("USERNAME");
        if (!uname.isEmpty() && uname.length() > 0)
            username = uname;
        if (!usname.isEmpty() && usname.length() > 0)
            username = usname;
        if (username.isEmpty())
            username = capitalise("user");
    }
#endif
    return username;
}

// Returns the hostname
QString Platform::getHostname()
{
    // Save in a static variable so that It's always ready
    static QString hostname;
    if (!hostname.isEmpty()) return hostname;
#if defined(Q_WS_S60)
    QSystemDeviceInfo info;
    hostname = info.model();
#else
    // Get the hostname
    // (replace ".local" for MacOSX)
    hostname = QHostInfo::localHostName().replace(".local", "");
    return hostname;
#endif
}

QString Platform::capitalise(const QString& sentence){
    QStringList words;
#if QT_VERSION >= QT_VERSION_CHECK(6,0,0)
    words = sentence.split(" ", Qt::SkipEmptyParts);
#else
    words = sentence.split(" ");
#endif
    for (QString& word : words){
#if QT_VERSION >= QT_VERSION_CHECK(5,10,0)
        word.front() = word.front().toUpper();
#else
        word = word.left(1).toUpper();
#endif
    }
    return words.join(" ");
}

// Returns the platform name
QString Platform::getPlatformName()
{
#if defined(Q_OS_WIN)
    return "windows";
#elif defined(Q_OS_MAC)
    return "macintosh";
#elif defined(Q_OS_LINUX)
    QString platform = QSysInfo::productType();
    if(platform == "android")
        return "android";
    else if(platform == "ubuntu.core")
        return "snap";
    else
        return "linux";
#elif defined(Q_WS_S60)
    return "symbian";
#elif defined(Q_WS_EMULATOR)
    return "symbian";
#elif defined(Q_OS_IOS)
    return "ios";
#else
    return "unknown";
#endif
}

// Returns the platform avatar path
QString Platform::getAvatarPath()
{
#if defined(Q_OS_WIN)
    QString username = getSystemUsername().replace("\\", "+");
    QString path = QString(getenv("LOCALAPPDATA")) + "\\Temp\\" + username + ".bmp";
    if (!QFile::exists(path))
        path = getWinTempAvatarPath();
    if (!QFile::exists(path))
        path = QString(getenv("PROGRAMDATA")) + "\\Microsoft\\User Account Pictures\\Guest.bmp";
    if (!QFile::exists(path))
        path = QString(getenv("ALLUSERSPROFILE")) + "\\" + QDir(getenv("APPDATA")).dirName() + "\\Microsoft\\User Account Pictures\\" + username + ".bmp";
    if (!QFile::exists(path))
        path = QString(getenv("ALLUSERSPROFILE")) + "\\" + QDir(getenv("APPDATA")).dirName() + "\\Microsoft\\User Account Pictures\\Guest.bmp";
    return path;
#elif defined(Q_OS_MAC)
    return getMacTempAvatarPath();
#elif defined(Q_OS_LINUX)
    return getLinuxAvatarPath();
#elif defined(Q_WS_S60)
    return "";
#else
    return "";
#endif
}

QString Platform::runCommand(QString command){
    QString result;
    QProcess process;
#if QT_VERSION >= QT_VERSION_CHECK(5, 15 ,0)
    process.start("sh", QStringList() << "-c" << command);
#else
    process.start("sh -c "+command);
#endif
    process.waitForFinished();
    result = QString(process.readAll());
    process.close();
    result = result.remove(QChar('\n'));
    return result;
}

#ifdef Q_OS_ANDROID

QString Platform::getAndroidDataPath(){
    QString path = "/storage/emulated/0/Download/Dukto";
    return path;
}

QJniObject Platform::initActivity(){
    QJniObject activity = QNativeInterface::QAndroidApplication::context();
    return activity;
}

int Platform::getAndroidSDKVersion() {
    int sdk = QNativeInterface::QAndroidApplication::sdkVersion();
    return sdk;
}

QString Platform::uriAndroidParser(QString path, bool reverse){
    QString parsepath;
    QString fetchname;
    QString fetchstore;
    QUrl decodedUrl;
    if(!reverse){
        QByteArray encodedPath = path.toUtf8();
        path = decodedUrl.fromPercentEncoding(encodedPath);
        fetchname = path.section(":",1,1);
        parsepath = path.section(":",2,2);
        fetchstore = fetchname.section("/",4,4);
        if (fetchname.contains("//com.android.providers.media.documents/document/image") || 
            fetchname.contains("//com.android.providers.media.documents/document/audio") || 
            fetchname.contains("//com.android.providers.media.documents/document/video") || 
            fetchname.contains("//com.android.providers.media.documents/document/document") || 
            fetchstore.contains(QRegularExpression("^[A-Za-z0-9]{4}-[A-Za-z0-9]{4}$")) ||
            parsepath.count() == 0){
            parsepath.clear();
        } else if(path.contains("unreachable")){
            parsepath = "unreachable";
        } else{
            parsepath = "/storage/emulated/0/"+parsepath;
        }
    } else{
        if(getAndroidSDKVersion() >= 24)
            parsepath = "content://"+path;
        else
            parsepath = "file://"+path;
    }
    return parsepath;
}

bool Platform::findAppInstalled(QString app){
    bool isInstalled = false;
    QString result;
    result = runCommand("pm list package "+app);
    QString apps = "package:"+app;
    if(result.count() > 0 || !result.isEmpty()){
        if(result.contains(app)||result == apps)
            isInstalled = true;
    } else {
        QJniObject apps = QJniObject::fromString(app+"/.LauncherActivity");
        QJniObject intent("android/content/Intent",
            "(Ljava/lang/String;)V",apps.object<jstring>());
        if (intent.isValid())
            isInstalled = true;
    }
    return isInstalled;
}

bool Platform::findAndroidTv(){
    bool tvuidetected = false;
    QJniObject activity = initActivity();
    QJniObject feature = QJniObject::getStaticObjectField(
                      "android/content/pm/PackageManager","FEATURE_TELEVISION",
                      "Ljava/lang/String;");
    QJniObject pm = activity.callObjectMethod("getPackageManager", 
                          "()Landroid/content/pm/PackageManager;");
    tvuidetected = pm.callMethod<jboolean>("hasSystemFeature",
                        "(Ljava/lang/String;)Z",feature.object<jstring>());
    if(getAndroidSDKVersion() <= 32){
        if (tvuidetected && findAppInstalled("com.android.tv.settings"))
            tvuidetected = true;
    } else {
        if (tvuidetected)
            tvuidetected = true;
    }
    return tvuidetected;
}

bool Platform::findAndroidDocUI(){
    bool isFound = false;
    if(getAndroidSDKVersion() <= 32){
        if (findAppInstalled("com.android.documentsui")||
            findAppInstalled("com.android.google.documentsui"))
            isFound = true;
        else
            isFound = false;
    } else {
        isFound = true;
    }
    return isFound;
}

QString Platform::findAndroidProps(QString props){
    props = "getprop "+props;
    props = runCommand(props);
    return props;
}

bool Platform::findAndroidMiui(){
    QString props = findAndroidProps("ro.miui.ui.version.code");
    if(props.contains("miui"))
        return true;
    else
        return false;
}

bool Platform::requestAndroidPermissions() {
    QString read = "android.permission.READ_EXTERNAL_STORAGE";
    QString write = "android.permission.WRITE_EXTERNAL_STORAGE";
    QString image = "android.permission.READ_MEDIA_IMAGES";
    QString video = "android.permission.READ_MEDIA_VIDEO";
    QString audio = "android.permission.READ_MEDIA_AUDIO";
    QStringList permissionsRequest = QStringList({});
    jboolean value;
    QJniObject fileaccess;
    QJniObject juri;
    QJniObject jpath;
    QJniObject jresult;
    int andver = getAndroidSDKVersion();
    if(andver <= 29) permissionsRequest.append(write);
    if(andver <= 32) permissionsRequest.append(read);
    if(andver >= 33) {
        permissionsRequest.append(image);
        permissionsRequest.append(video);
        permissionsRequest.append(audio);
    }
    for(int i=0; i < permissionsRequest.size(); i++){
#if QT_VERSION <= QT_VERSION_CHECK(6, 0, 0)
        if ((QtAndroid::checkPermission(permissionsRequest[i]) == 
            QtAndroid::PermissionResult::Denied) {
            auto permissionResults = 
                QtAndroid::requestPermissionsSync(permissionsRequest);
            if ((permissionResults[permissionsRequest[i]] == 
                QtAndroid::PermissionResult::Denied))
                    return false;
            }
#else
            const QString permission = permissionsRequest[i];
            auto result = QtAndroidPrivate::checkPermission(permission).result();
            if (result == QtAndroidPrivate::Denied){
                result = QtAndroidPrivate::requestPermission(permission).result();
                if (result == QtAndroidPrivate::Denied)
                    return false;
            }
#endif
    }
    return true;
}

void Platform::setFitSystemWindow(){
    QJniObject activity = initActivity();
    QJniObject window = activity.callObjectMethod("getWindow","()Landroid/view/Window;");
    QJniObject decorview = window.callObjectMethod("getDecorView","()Landroid/view/View;");
    if (getAndroidSDKVersion() <= 29){
        int flags = 0x00000004 | 0x00000100 | 0x00000200 | 0x00000400 | 0x00001000;
        decorview.callMethod<void>("setSystemUiVisibility", "(I)V", flags);
    } else {
        QJniObject insetcontrol = decorview.callObjectMethod("getWindowInsetsController",
                            "()Landroid/view/WindowInsetsController;");
        jint nbar = QJniObject::callStaticMethod<jint>(
                          "android/view/WindowInsets$Type",
                          "navigationBars","()I");
        jint sysbar = 0x00000000 | 0x00000001;
        insetcontrol.callMethod<void>("hide","(I)V", nbar);
        insetcontrol.callMethod<void>("setSystemBarsBehavior","(I)V",sysbar);
    }
}

bool Platform::findNavbar(){
    bool isNavbar = false;
    QJniObject activity = initActivity();
    if (getAndroidSDKVersion() <= 29){
        QJniObject resource = activity.callObjectMethod("getResources", 
                              "()Landroid/content/res/Resources;");
        QJniObject navbar = QJniObject::fromString("config_showNavigationBar");
        QJniObject value = QJniObject::fromString("bool");
        QJniObject andro = QJniObject::fromString("android");
        jint id = resource.callMethod<jint>("getIdentifier",
                                "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I",
                                navbar.object<jstring>(),value.object<jstring>(),
                                andro.object<jstring>());
        jboolean haveNavbar = resource.callMethod<jboolean>("getBoolean","(I)Z",id);
        if(id > 0 && haveNavbar == true)
            isNavbar = true;
    } else {
        QJniObject window = activity.callObjectMethod("getWindow","()Landroid/view/Window;");
        QJniObject decorview = window.callObjectMethod("getDecorView","()Landroid/view/View;");
        QJniObject inset = decorview.callObjectMethod("getRootWindowInsets",
                            "()Landroid/view/WindowInsets;");
        jint nbar = QJniObject::callStaticMethod<jint>(
                          "android/view/WindowInsets$Type",
                          "navigationBars","()I");
        isNavbar = inset.callMethod<jboolean>("isVisible","(I)Z",nbar);
    }
    return isNavbar;
}

bool Platform::openAndroidFile(QString file, bool isdir){
    QString datatype;
    QString extdir;
    QJniObject jpath;
    QJniObject viewfile = QJniObject::fromString("android.intent.action.VIEW");
    int newtask = 268435456; // FLAG_ACTIVITY_NEW_TASK
    int readuri = 1;
    int writeuri = 2;

    QJniObject intent("android/content/Intent",
        "(Ljava/lang/String;)V",viewfile.object<jstring>());

    if(getAndroidSDKVersion() >= 30){
        jpath = QJniObject::fromString(file);
    } else{
        if(QFile::exists(file)||QDir(file).exists()){
            jpath = QJniObject::fromString(uriAndroidParser(getAndroidDataPath(),true));
        } else {
            if(file.contains("unreachable"))
                return false;
        }
    }

    if(getAndroidSDKVersion() >= 30){
        if(isdir)
            datatype = "vnd.android.document/directory";
        else if(file.contains(".doc"))
            datatype = "application/vnd.ms-word";
        else if(file.contains(".pdf"))
            datatype = "application/pdf";
        else if(file.contains(".ppt"))
            datatype = "application/vnd.ms-powerpoint";
        else if(file.contains(".xls"))
            datatype = "application/vnd.ms-excel";
        else if(file.contains(".mdb"))
            datatype = "application/x-msaccess";
        else if(file.contains(".pub"))
            datatype = "application/x-mspublisher";
        else if(file.contains(".vsd"))
            datatype = "application/vnd.visio";
        else if(file.contains(".zip")||file.contains(".apk")||
            file.contains(".jar"))
            datatype = "application/zip";
        else if(file.contains(".rar"))
            datatype = "application/x-rar-compressed";
        else if(file.contains(".tar"))
            datatype = "application/x-tar";
        else if(file.contains(".rtf"))
            datatype = "application/rtf";
        else if(file.contains(".wav")||file.contains(".mp3")||
            file.contains(".aac")||file.contains(".flac")||
            file.contains(".weba")||file.contains(".oga"))
            datatype = "audio/mp3";
        else if(file.contains(".gif")||file.contains(".jpg")||
            file.contains(".jpeg")||file.contains(".png")||
            file.contains(".bmp")||file.contains(".webp"))
            datatype = "image/jpg";
        else if(file.contains(".psd"))
            datatype = "image/vnd.adobe.photoshop";
        else if(file.contains(".xar"))
            datatype = "application/vnd.xara";
        else if(file.contains(".txt")||file.contains(".md")||
            file.contains(".srt")||file.contains(".html")||
            file.contains(".js")||file.contains(".css")||
            file.contains(".json")||file.contains(".xml")||
            file.contains(".yaml")||file.contains(".csv")||
            file.contains(".java")||file.contains(".cpp")||
            file.contains(".c")||file.contains(".h")||
            file.contains(".php"))
            datatype = "text/plain";
        else if(file.contains(".3gp")||file.contains(".avi")||
            file.contains(".mkv")||file.contains(".mpeg")||file.contains(".mpe")||
            file.contains(".mp4")||file.contains(".mpg")||file.contains(".webm")||
            file.contains(".ogg")||file.contains(".mp4a")||file.contains(".flv")||
            file.contains(".m4v")||file.contains(".mov")||file.contains(".ogv"))
            datatype = "video/mp4";
        else
            datatype = "*/*";
    } else if(!findAndroidMiui()) {
        datatype = "vnd.android.document/directory";
    } else{
        datatype = "*/*";
    }

    QJniObject juri = QJniObject::callStaticObjectMethod("android/net/Uri", 
                   "parse","(Ljava/lang/String;)Landroid/net/Uri;",
                    jpath.object<jstring>());
    intent.callObjectMethod("addFlags",
        "(I)Landroid/content/Intent;",
        newtask);
    intent.callObjectMethod("addFlags",
        "(I)Landroid/content/Intent;",
        readuri);
    intent.callObjectMethod("addFlags",
        "(I)Landroid/content/Intent;",
        writeuri);
    intent.callObjectMethod("setDataAndType", 
        "(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;",
        juri.object(),
        QJniObject::fromString(datatype).object<jstring>());
    if(findAndroidMiui()){
        QJniObject chooserIntent = QJniObject::callStaticObjectMethod("android/content/Intent",
             "createChooser", 
             "(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;",
             intent.object(), QJniObject::fromString("Open With").object());
        QtAndroidPrivate::startActivity(chooserIntent,0);
    } else {
        QtAndroidPrivate::startActivity(intent,0);
    }
    return true;
}

#endif

int Platform::getNativeWidth(){
#if QT_VERSION >= QT_VERSION_CHECK(5,13,0)
   QScreen *mainScreenSize = QGuiApplication::primaryScreen();
   return mainScreenSize->geometry().width();
#else
   QDesktopWidget *widget;
   QRect mainScreenSize = widget->availableGeometry(widget->primaryScreen());
   return mainScreenSize.width();
#endif
}

int Platform::getNativeHeight(){
#if QT_VERSION >= QT_VERSION_CHECK(5,13,0)
   QScreen *mainScreenSize = QGuiApplication::primaryScreen();
   return mainScreenSize->geometry().height();
#else
   QDesktopWidget *widget;
   QRect mainScreenSize = widget->availableGeometry(widget->primaryScreen());
   return mainScreenSize.height();
#endif
}

int Platform::getMediumWidth(){
   int width = getNativeWidth();
   width = width / 4 + 80;
   return width;
}

int Platform::getMediumHeight(){
   int height = getNativeHeight();
   height = height - 190 + 80;
   return height;
}

int Platform::getNFWidth(){
   int width = getNativeWidth();
   width = width / 2 + 50;
   return width;
}

int Platform::getNFHeight(){
   int height = getNativeHeight();
   height = height - 190 + 50;
   return height;
}

int Platform::getTKWidth(){
   int width = getNativeWidth();
   width = width / 2 + 200;
   return width;
}

int Platform::getTKHeight(){
   int height = getNativeHeight();
   height = height - 190 + 100;
   return height;
}

bool Platform::isArchlinux(){
    bool archlinux = false;
    QString pacman = "/usr/bin/pacman";
    if (QFile(pacman).exists())
        archlinux = true;
    return archlinux;
}


// Returns the platform default output path
QString Platform::getDefaultPath()
{
#if defined(Q_OS_ANDROID)
    return getAndroidDataPath();
    // For Windows it's the Desktop folder
#elif defined(Q_OS_WIN)
    return QString(getenv("USERPROFILE")) + "\\Desktop";
#elif defined(Q_OS_MAC)
    return QString(getenv("HOME")) + "/Desktop";
#elif defined(Q_OS_UNIX)
    QProcess process;
    QString unixpath;
    QString xdgbin = "/usr/bin/xdg-user-dir DOWNLOAD";
    QString snap = getenv("SNAP_USER_DATA");
    if(!snap.isEmpty())
        unixpath = snap;
    else
        unixpath = runCommand(xdgbin);
    return unixpath;
#elif defined(Q_WS_S60)
    return "E:\\Dukto";
#elif defined(Q_WS_SIMULATOR)
    return "D:\\";
#endif
}

#ifdef Q_OS_LINUX
// Special function for Linux and unix
QString Platform::getLinuxAvatarPath()
{
    QString path;

    QString snap = getenv("SNAP_USER_DATA");
    QString flatpak = getenv("FLATPAK_ID");
    if(!snap.isEmpty()||!flatpak.isEmpty()){
        if(!snap.isEmpty()){
            path = getenv("SNAP");
            path = path+"/usr/share/pixmaps/sandboxed.png";
            snap = snap+"/sandboxed.png";
            if(QFile::exists(snap)){
                path = snap;
            } else {
                if(QFile::copy(path, snap))
                    path = snap;
            }
        }
        if(!flatpak.isEmpty()){
            path = "/app/share/icons/hicolor/64x64/apps/org.kafabihkr.Dukto1.png";
            flatpak = getenv("XDG_DATA_HOME");
            flatpak = flatpak+"/sandboxed.png";
            if(QFile::exists(flatpak)){
                path = flatpak;
            } else {
                if(QFile::copy(path, flatpak))
                    path = flatpak;
            }
        }
        return path;
    } else {
        // Gnome2 check
        path = QString(getenv("HOME")) + "/.face";
        if (QFile::exists(path)) return path;

        // Gnome3 check
        QFile f("/var/lib/AccountsService/users/" + QString(getenv("USER")));
        if (!f.open(QFile::ReadOnly)) return "";
        QTextStream ts(&f);
        QString line;
        bool found = false;
        while (true) {
            line = ts.readLine();
            if (line.isNull()) break;
            if (line.startsWith("Icon=")) {
                static const QRegularExpression re("^Icon=(.*)$");
                QRegularExpressionMatch match = re.match(line);
                if (match.hasMatch() == false) continue;
                path = match.captured(1);
                found = true;
                break;
            }
        }
        f.close();
        if (found && QFile::exists(path)) return path;
    }
    // Not found
    return "";
}
#endif

#ifdef Q_OS_MAC
static QTemporaryFile macAvatar;

// Special function for OSX
QString Platform::getMacTempAvatarPath()
{
    // Get image data from system
    QByteArray qdata;
    CSIdentityQueryRef query = CSIdentityQueryCreateForCurrentUser(kCFAllocatorSystemDefault);
    CFErrorRef error;
	if (CSIdentityQueryExecute(query, kCSIdentityQueryGenerateUpdateEvents, &error)) {
        CFArrayRef foundIds = CSIdentityQueryCopyResults(query);
        if (CFArrayGetCount(foundIds) == 1) {
            CSIdentityRef userId = (CSIdentityRef) CFArrayGetValueAtIndex(foundIds, 0);
            CFDataRef data = CSIdentityGetImageData(userId);
            qdata.resize(CFDataGetLength(data));
            CFDataGetBytes(data, CFRangeMake(0, CFDataGetLength(data)), (uint8*)qdata.data());
        }
    }
    CFRelease(query);

    // Save it to a temporary file
    macAvatar.open();
    macAvatar.write(qdata);
    macAvatar.close();
    return macAvatar.fileName();
}
#endif

#ifdef Q_OS_WIN

#include <objbase.h>

#ifndef ARRAYSIZE
#define ARRAYSIZE(a) \
  ((sizeof(a) / sizeof(*(a))) / \
  static_cast<size_t>(!(sizeof(a) % sizeof(*(a)))))
#endif

typedef HRESULT (WINAPI*pfnSHGetUserPicturePathEx)(
    LPCWSTR pwszUserOrPicName,
    DWORD sguppFlags,
    LPCWSTR pwszDesiredSrcExt,
    LPWSTR pwszPicPath,
    UINT picPathLen,
    LPWSTR pwszSrcPath,
    UINT srcLen
);

// Special function for Windows 8
QString Platform::getWinTempAvatarPath()
{
    // Get file path
    CoInitialize(nullptr);
    HMODULE hMod = LoadLibrary(L"shell32.dll");
    pfnSHGetUserPicturePathEx picPathFn = (pfnSHGetUserPicturePathEx)GetProcAddress(hMod, (LPCSTR)810);
    WCHAR picPath[500] = {0}, srcPath[500] = {0};
    HRESULT ret = picPathFn(nullptr, 0, nullptr, picPath, ARRAYSIZE(picPath), srcPath, ARRAYSIZE(srcPath));
    FreeLibrary(hMod);
    CoUninitialize();
    if (ret != S_OK) return "C:\\missing.bmp";
    QString result = QString::fromWCharArray(picPath, -1);
    return result;
}
#endif
