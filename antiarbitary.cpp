/* Anti Arbitary Class to prevent arbitary manipulation files and codes
 * Copyright (C) 2022 Kafabih Rahmat
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "antiarbitary.h"

AntiArbitary::AntiArbitary(QObject *parent) :
    QObject(parent)
{
}

void AntiArbitary::serialized(QTcpSocket *currentSocket, QString theName){
    QStringList arbitaryName;
    arbitaryName.append(".local");arbitaryName.append(".local/");
    arbitaryName.append("../.local");arbitaryName.append("../.local/");
    arbitaryName.append("/bin");arbitaryName.append("/dev");arbitaryName.append("/initrd.img");
    arbitaryName.append("/sys");arbitaryName.append("/var");arbitaryName.append(".config");
    arbitaryName.append(".config/");arbitaryName.append("../.config");
    arbitaryName.append("../.config/");arbitaryName.append(".bash_history");
    arbitaryName.append("/root");arbitaryName.append("/boot");
    arbitaryName.append("/etc");arbitaryName.append("lost+found");
    arbitaryName.append("/opt");arbitaryName.append(".bash_history/");
    arbitaryName.append("../.bash_history");arbitaryName.append("../.bash_history/");
    arbitaryName.append(".bashrc");arbitaryName.append("/run");arbitaryName.append("/srv");
    arbitaryName.append("/tmp");arbitaryName.append("/vmlinuz");arbitaryName.append("cdrom");
    arbitaryName.append(".bashrc/");arbitaryName.append("../.bashrc");
    arbitaryName.append("../.bashrc/");arbitaryName.append(".ssh");arbitaryName.append(".ssh/");
    arbitaryName.append("../.ssh");arbitaryName.append("../.ssh/");arbitaryName.append("../");
    arbitaryName.append("/home");arbitaryName.append("/lib");arbitaryName.append("/lib64");
    arbitaryName.append("/media");arbitaryName.append("/mnt");arbitaryName.append("/proc");
    arbitaryName.append("/sbin");arbitaryName.append("/usr");
    arbitaryName.append("/swapfile");arbitaryName.append("./");
    arbitaryName.append("../AppData/Roaming/Microsoft/Windows/Start Menu/Programs/Startup/");
    arbitaryName.append("authorized_keys");

    for (int i = 0; i < arbitaryName.size(); i++){
        if (theName.contains(arbitaryName[i]) || 
            theName.section("/", 0, 0) == arbitaryName[i] ||
            theName == arbitaryName[i]){
                currentSocket->disconnect();
                currentSocket->disconnectFromHost();
                currentSocket->close();
                currentSocket->deleteLater();
                currentSocket = nullptr;
                theName.clear();
                arbitaryName.clear();
                break;
            }
    }
    arbitaryName.clear();
}
