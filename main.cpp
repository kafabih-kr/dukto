/* DUKTO - A simple, fast and multi-platform file transfer tool for LAN users
 * Copyright (C) 2011 Emanuele Colombo
 * Copyright (C) 2015 Arthur Zamarin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <QApplication>
#include <QCommandLineParser>
#include <QSplashScreen>
#include "systemtray.h"

#include "settings.h"
#include "guibehind.h"
#include "duktowindow.h"

#include "platform.h"

#ifdef SINGLE_APP
    #include <singleapplication.h>
    #ifdef Q_OS_WIN
        #include <windows.h>
    #endif
    #if defined(Q_WS_S60)
        #define SYMBIAN
    #endif
    #if defined(Q_WS_SIMULATOR)
        #define SYMBIAN
    #endif
#endif

int main(int argc, char *argv[])
{
#ifdef Q_OS_WIN
    qputenv("QML_ENABLE_TEXT_IMAGE_CACHE", "true");
    if(QSysInfo::productVersion() == "7" || QSysInfo::productVersion() == "7 SP 1" ||
       QSysInfo::productVersion() == "8" || QSysInfo::productVersion() == "8.1"){
        qputenv("QML_NO_THREADED_RENDERER","true");
        qputenv("QSG_RENDER_LOOP","basic");
    }
#endif
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
    qputenv("QT_SCALE_FACTOR", "1");
#ifdef Q_OS_WIN
    qputenv("QSG_RHI_BACKEND", "angle");
    qputenv("QT_ANGLE_PLATFORM","d3d9");
#else
    qputenv("QSG_RHI_BACKEND", "gl");
#endif
#endif

#ifdef Q_OS_ANDROID
    for (int i = 0; i < 100000; ++i)
       qDebug() << "Dukto is loading " << i;
#endif

#ifndef SINGLE_APP
    QApplication app(argc, argv);
    Platform *pl;
#if QT_VERSION <= QT_VERSION_CHECK(6, 0, 0)
    if(pl->getNativeWidth() >= 1280)
        app.setAttribute(Qt::AA_EnableHighDpiScaling, true);
#endif
    QPixmap pixmap(":/splash.png");
    QSplashScreen splash(pixmap);
#else
    // Check for single running instance
    SingleApplication app(argc, argv, true);
    if (app.isSecondary()) {
#ifdef Q_OS_WIN
        AllowSetForegroundWindow(static_cast<DWORD>(app.primaryPid()));
#endif
        app.sendMessage(QByteArray("A"));
        return 0;
    }
#endif

    QString product;
    if(!pixmap.isNull()){
        splash.show();
        product = QSysInfo::productType();
        splash.showMessage("Dukto is\nrunning for "+product+" users,\nplease wait...");
    }

    QOpenGLContext opengles;
#ifdef Q_OS_ANDROID
    if(opengles.isOpenGLES() && !pl->findAndroidMiui())
        app.setAttribute(Qt::AA_UseOpenGLES, true);
#else
    if(opengles.isOpenGLES())
        app.setAttribute(Qt::AA_UseOpenGLES, true);
#endif

    QCommandLineParser parser;
    parser.setApplicationDescription("Dukto is a simple, fast and multi-platform file transfer tool for LAN users.");
    parser.addHelpOption();
    QCommandLineOption hideOption(QStringList() << "H" << "hide", "Hide when launched");
    parser.addOption(hideOption);
    parser.process(app);

    if(!pixmap.isNull()){
        splash.showMessage("Dukto is\nsetting for "+product+" systems, \nplease wait...");
    }
    Settings settings;
    GuiBehind gb(&settings);
    app.installEventFilter(&gb);

    if(!pixmap.isNull()){
        splash.showMessage("Dukto is\ndrawing for "+product+" system, \nplease wait...");
    }

    DuktoWindow viewer(&gb, &settings);
#ifdef SINGLE_APP
    #ifndef SYMBIAN
        app.setActivationWindow(&viewer, true);
    #endif
    #ifndef Q_WS_S60
        viewer.showExpanded();
	app.installEventFilter(&gb);
    #else
    QObject::connect(&app, &SingleApplication::receivedMessage, [&viewer]()->void {
        if (viewer.isMinimized())
        {
            viewer.showNormal();
        }
        viewer.activateWindow();
    });
    #endif
#endif

    if(!pixmap.isNull()){
        splash.showMessage("Dukto is\nfinishing loading on "+product+" system, \nplease wait...");
    }

    SystemTray tray(viewer);
    gb.setViewer(&viewer, &tray);
    viewer.setVisible(!parser.isSet(hideOption));
    tray.show();
    if(!pixmap.isNull()){
        splash.showMessage("Dukto is\nfinished loading on "+product+" system, \nwaiting time is over");
        splash.finish(&viewer);
    }
    gb.showRequest();
    return app.exec();
}
