/* DUKTO - A simple, fast and multi-platform file transfer tool for LAN users
 * Copyright (C) 2011 Emanuele Colombo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

import QtQuick 2.3

Rectangle {
    id: mainElement
    focus: true

    FontLoader {
        id: duktofont
        source: "Klill-Light.ttf"
    }

    FontLoader {
        id: duktofontsmall
        source: "LiberationSans-Regular.ttf"
    }

    FontLoader {
        id: duktofonthappy
        source: "KGLikeASkyscraper.ttf"
    }

    Connections {
         target: guiBehind
         function onTransferStart() {
             duktoOverlay.state = "progress"
         }
         function onReceiveCompleted() {
             duktoOverlay.state = ""
             duktoInner.gotoPage("recent");
         }
         function onGotoTextSnippet() {
             duktoOverlay.state = "showtext"
         }
         function onGotoSendPage() {
             duktoOverlay.state = "send";
         }
         function onGotoMessagePage() {
             duktoOverlay.state = "message";
         }
         function onHideAllOverlays() {
             duktoOverlay.state = "";
         }
    }

    DuktoInner {
        id: duktoInner
        anchors.fill: parent
        Connections {
            function onShowIpList() {
                duktoOverlay.state = "ip"
            }
            function onShowSettings() {
                duktoOverlay.refreshSettingsColor();
                duktoOverlay.state = "settings";
            }
        }
    }

    UpdatesBox {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
    }

    DuktoOverlay {
        id: duktoOverlay
        anchors.fill: parent
    }

    Binding {
        target: guiBehind
        property: "overlayState"
        value: duktoOverlay.state
    }
    
    property variant innerState : ["buddies", "recent", "about"]
    property variant tbState : ["","ip","settings","destination"]
    property variant buddiesState : ["","custom"]
    property variant tbPageIndex: duktoOverlay.state
    property variant sendPageDropText: duktoOverlay.spLabelDrop
    property int innerIndex : 0
    property int tbIndex : 0
    property int buddiesIndex : 0

    Component.onCompleted:{
        //buddiesState.append(buddiesListData)
        if(!guiBehind.sendFolderAvailable){
            sendPageDropText.text = sendPageDropText.text + 
                "\n\n\nSorry for Android users which is using"+
                "\nabove version 10. Because the new"+
                "\nAndroid API is applied Storage Access"+
                "\nFramework (SAF), there was some features"+
                "\nis disabled such as multiple file send"+
                "\nand folder send. you can only share one"+
                "\nfile only, and you cannot enabled these"+
                "\nfeatures."
        }
        if(guiBehind.androidTvAvailable == true){
            duktoInner.bsText.text = "Sorry, for Android TV users."+
                "\nRecents Received files feature is not available,"+
                "\nplease use open destination folder feature instead"
            duktoInner.bsText.visible = 1
        }
    }

    Keys.onPressed: function (event){
        if (event.key === Qt.Key_Down) {
            if(duktoInner.tbPageState === "WithoutLabels"){
                duktoInner.tbPageState = "WithLabels"
                duktoInner.state = "WithLabels"
            } else {
                duktoInner.tbPageState = "WithoutLabels"
                duktoInner.state = "WithoutLabels"
            }
        }
        if(duktoInner.tbPageState === "WithoutLabels"){
            duktoInner.state = innerState[innerIndex]
            if (event.key === Qt.Key_Right) {
                if (innerState[innerIndex] === duktoInner.state) {
                    innerIndex++
                    if(innerIndex !== innerState.length && innerIndex > 0){
                        duktoInner.gotoPage(innerState[innerIndex])
                    }
                    if(innerIndex >= innerState.length){
                        innerIndex = 0
                        duktoInner.gotoPage(innerState[innerIndex])
                    }
                }
            }
            if (event.key === Qt.Key_Left) {
                if (innerState[innerIndex] === duktoInner.state) {
                    innerIndex--
                    if(innerIndex !== innerState.length && innerIndex != -1){
                        duktoInner.gotoPage(innerState[innerIndex])
                    }
                    if(innerIndex == -1){
                        innerIndex = innerState.length - 1
                        duktoInner.gotoPage(innerState[innerIndex])
                    }
                }
            }
            if(duktoInner.state === "buddies"){
                if (event.key === Qt.Key_Up) {
                
                }
            }
            if(duktoInner.state === "recent"){
                
            }
        }
        if(duktoInner.tbPageState === "WithLabels"){
            duktoOverlay.state = tbState[tbIndex]
            if (event.key === Qt.Key_Right) {
                if (tbState[tbIndex] === duktoOverlay.state) {
                    tbIndex++
                    if(tbIndex !== tbState.length && tbIndex > 0){
                        duktoOverlay.state = tbState[tbIndex]
                    }
                    if(tbIndex == 3){
                        guiBehind.openDestinationFolder()
                    }
                    if(tbIndex >= tbState.length){
                        tbIndex = 0
                        duktoOverlay.state = tbState[tbIndex]
                    }
                }
            }
            if (event.key === Qt.Key_Left) {
                if (tbState[tbIndex] === duktoOverlay.state) {
                    tbIndex--
                    if(tbIndex !== tbState.length && tbIndex!= -1){
                        duktoOverlay.state = tbState[tbIndex]
                    }
                    if(tbIndex == -1){
                        tbIndex = tbState.length - 1
                        duktoOverlay.state = tbState[tbIndex]
                    }
                    if(tbIndex == 3){
                        guiBehind.openDestinationFolder()
                    }
                }
            }
        }
    }
}
