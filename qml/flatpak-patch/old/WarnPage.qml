/* DUKTO - A simple, fast and multi-platform file transfer tool for LAN users
 * Copyright (C) 2022 Kafabih Rahmat
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

import QtQuick 2.3

Rectangle {
    id: warnPage
    color: "#ffffff"

    signal ok()

    MouseArea {
        anchors.fill: parent
    }

    SmoothText {
        id: boxTitle
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 15
        anchors.topMargin: 5
        font.pixelSize: 64
        text: "Warning!"
        color: theme.color3
    }

    SText {
        id: boxSender
        anchors.left: boxTitle.left
        anchors.top: parent.top
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.topMargin: 45
        font.pixelSize: 16
        text: "Please read and press 'Accept'"
        color: "#888888"
    }


    Rectangle {
        id: rectangleText
        border.color: theme.color3
        border.width: 1
        anchors.top: boxSender.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 15
        anchors.bottom: buttonCopy.top
        anchors.bottomMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 10

        Flickable {
            id: flickableText
            anchors.fill: parent
            anchors.margins: 5
            contentHeight: textEditSnippet.paintedHeight
            
            clip: false

            function ensureVisible(r) {
                if (contentX >= r.x)
                    contentX = r.x;
                else if (contentX+width <= r.x+r.width)
                    contentX = r.x+r.width-width;
                if (contentY >= r.y)
                    contentY = r.y;
                else if (contentY+height <= r.y+r.height)
                    contentY = r.y+r.height-height;
            }

            TextEdit {
                id: textEditSnippet
                color: "#888888"
                width: rectangleText.width - 10
                font.family: duktofont.name
                font.pixelSize: 37
                selectByMouse: false
                wrapMode: TextEdit.Wrap
                textFormat: TextEdit.PlainText
                readOnly: true
                text: "Please use carefully of Dukto flatpak version cause of security sanboxed app issue. Want to use secured Dukto? Please install Dukto as snap."
                
            }
        }
    }

    ButtonDark {
        id: buttonCopy
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 10
        anchors.bottomMargin: 10
        buttonEnabled: true
        label: "Accept"
        onClicked: warnPage.ok();
    }
}

