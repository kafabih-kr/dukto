![featured](https://kafabih-kr.gitlab.io/kr-droid-app-collections/fdroid/repo/org.kafabihkr.Dukto/en-US/featureGraphic.png)

# Dukto
Dukto project from source forge: http://sourceforge.net/p/dukto/

Dukto is now ported to QT 6 and still keeping Symbian code and additional port.

## firewall rules

Dukto uses **TCP** or **UDP** port **4644** for receiving a message. so, 
be aware to open 4644 to everybody on the LAN.

If you don't do so, you'll not be able to see any other buddies on the LAN.

for example, shorewall
```bash
cd /etc/shorewall
sudo vim rules
```
add the following lines:

```ini
#dukto r6
ACCEPT          net             $FW             tcp             4644
ACCEPT          net             $FW             udp             4644
```
then, 
```bash
sudo sytemctl restart shorewall
```

iptables directly :

```bash
/sbin/iptables -I INPUT -p tcp --dport 4644 -j ACCEPT 
/sbin/iptables -I INPUT -p udp --dport 4644 -j ACCEPT 
```
Windows or Mac users just do the same job as above.


thanks to [Emanuele Colombo](http://sourceforge.net/u/colomboe/) for bringing us this wonderful software!


For supporting Dukto to the next versions, now accepted donation by using Indonesian Rupiah's by clicking [here](https://saweria.co/kafnix) or by scanning QR Code as below.


![saweria](./qml/common/saweriakafabih.png?raw=true "saweria")
